#!/usr/bin/env bash

set -e

# export UTOPIA_FE_EXEC=/Users/patrickzulian/Desktop/code/utopia/utopia_fe/build/utopia_fe_exec
# export UTOPIA_FE_EXEC=/Users/patrickzulian/Desktop/code/utopia/utopia_fe/build_debug/utopia_fe_exec
# export UTOPIA_SRC_DIR=/Users/patrickzulian/Desktop/code/utopia

if [[ -z "$UTOPIA_SRC_DIR" ]]
then
	echo "Error! Please define UTOPIA_SRC_DIR=<path_to_source_dir>"
	exit -1
fi

if [[ -z "$TRILINOS_DIR" ]]
then
	echo "Error! Please define TRILINOS_DIR=<path_to_source_dir>"
	exit -1
fi

N_PROCS=8
export COST_ESTIMATION_N_PROCS=$N_PROCS

#CASE_DIR=$PWD/../../fracnetflow/3D/benchmarks/benchmark_1_ED/small
# CASE_DIR=$PWD/../../fracnetflow/3D/benchmarks/benchmark_1/large 
CASE_DIR=$PWD/../../fracnetflow/3D/benchmarks/benchmark_3/small
# CASE_DIR=$PWD/../../fracnetflow/3D/benchmarks/benchmark_1/large 
# CASE_DIR=$PWD/../../fracnetflow/3D/benchmarks/benchmark_4/large 

PATH=$UTOPIA_SRC_DIR/utopia_fe/workflows/FSI/:$PWD:$PATH

clean_meshes.sh

set -x

export DISPLACE_SOLID=false
export RESCALE_IMBALANCE=2


static_load_balancing.sh $CASE_DIR/mesh_matrix.e $CASE_DIR/mesh_fracture.e $N_PROCS
# $TRILINOS_DIR/bin/decomp -p $N_PROCS --spectral $CASE_DIR/mesh_matrix.e

$TRILINOS_DIR/bin/decomp -p $N_PROCS --spectral $CASE_DIR/mesh_fracture.e


# bat $CASE_DIR/../flow.yaml
# --oversubscribe 
mpiexec -np $N_PROCS $UTOPIA_FE_EXEC @file $CASE_DIR/../flow.yaml -on_error_attach_debugger lldb
