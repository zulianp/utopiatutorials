#!/usr/bin/env bash

# set -e
set -x

rm mesh_*.e.* 
rm porous_*.e.*
rm fracture_network_out.e*
rm imbalance.e cost.e indicator.e
rm imbalance.e.* cost.e.* indicator.e.*
rm cost.e
rm cost.e.*

rm -rf workspace
