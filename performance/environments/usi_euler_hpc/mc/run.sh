#!/bin/bash
source init.sh

cd $USER_WORK_DIR

# cd test
# sbatch test.sbatch
# cd $USER_WORK_DIR


# cd mars/poisson
# sbatch poisson.sbatch

# cd $USER_WORK_DIR

cd elasticity
sbatch elasticity.sbatch

# cd $USER_WORK_DIR

# cd mars/thermo_elasticity
# sbatch thermo_elasticity.sbatch

cd $USER_WORK_DIR

