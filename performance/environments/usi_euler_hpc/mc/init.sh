
#!/bin/bash
export USER_WORK_DIR=$PWD
export CASES_DIR=$PWD/../../cases

# load env
#source $APPS/UES/anfink/cpu/environment
source env_MC.sh


export INTREPID2_DIR=$TRILINOS_DIR/lib/cmake/Intrepid2
export LD_LIBRARY_PATH=$LIBMESH_DIR/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PETSC_DIR/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$TRILINOS_DIR/lib:$LD_LIBRARY_PATH

#export OMP_PROC_BIND=true

# Installation directories
export INSTALL_DIR=$USER_WORK_DIR/installations/

export YAML_CPP_DIR=$INSTALL_DIR/yaml-cpp
export MARS_DIR=$INSTALL_DIR/mars
export MOONOLITH_DIR=$INSTALL_DIR/par_moonolith
export UTOPIA_DIR=$INSTALL_DIR/utopia
export UTOPIA_FE_DIR=$INSTALL_DIR/utopia_fe
export TRILINOS_DIR=$INSTALL_DIR/Trilinos
export PETSC_DIR=$INSTALL_DIR/petsc

export UTOPIA_FE_EXEC_MC=$UTOPIA_SOURCE_DIR/utopia_fe/build_cpu/utopia_fe_exec
export UTOPIA_FE_TEST_MC=$UTOPIA_SOURCE_DIR/utopia_fe/build_cpu/utopia_fe_test

# Usefull binaries
# export KOKKOS_PROFILE_LIBRARY=$HOME/installations/kokkos-tools/kp_kernel_timer.so
