#!/bin/bash
source init.sh

########################## PETSC ##########################################

unset PETSC_DIR

mkdir $UTOPIA_SOURCE_DIR/utopia/build_petsc_cpu
cd $UTOPIA_SOURCE_DIR/utopia/build_petsc_cpu &&  \
     cmake .. -DUTOPIA_DEPENDENCIES_DIR=$INSTALL_DIR \
	      -DUTOPIA_ENABLE_PETSC_DM_PLEX=ON \
     && make -j16 petsc

export PETSC_DIR=$INSTALL_DIR/petsc



########################## TRILINOS ##########################################
mkdir $TRILINOS_SOURCE_DIR/build_cpu
cd $TRILINOS_SOURCE_DIR/build_cpu && source $UTOPIA_SOURCE_DIR/utopia/scripts/installer/configure_trilinos.sh; \
cd $TRILINOS_SOURCE_DIR/build_cpu && cmake -DNetcdf_LIBRARY_DIRS=$PETSC_DIR/lib -DNetcdf_INCLUDE_DIRS=$PETSC_DIR/include .. \
&& make -j16 && make install


########################## MARS ##########################################

mkdir $MARS_SOURCE_DIR/build_cpu
cd $MARS_SOURCE_DIR/build_cpu && cmake .. \
-DCMAKE_VERBOSE_MAKEFILE=ON  \
-DCMAKE_BUILD_TYPE=Release  \
-DCMAKE_INSTALL_PREFIX=$MARS_DIR \
&& make -j16 && make install

########################## YAML ##########################################

mkdir $YAML_CPP_SOURCE_DIR/build_cpu
cd $YAML_CPP_SOURCE_DIR/build_cpu && cmake .. \
    -DCMAKE_INSTALL_PREFIX=$YAML_CPP_DIR \
    -DCMAKE_BUILD_TYPE=Release  \
    && make -j16 && make install

########################## MOONOLITH ##########################################

mkdir $MOONOLITH_SOURCE_DIR/build_cpu
cd $MOONOLITH_SOURCE_DIR/build_cpu && cmake .. -DCMAKE_INSTALL_PREFIX=$MOONOLITH_DIR && \
 make -j && make install

 ########################## UTOPIA ##########################################

## ALGEBRA

cd $UTOPIA_SOURCE_DIR/utopia && mkdir $UTOPIA_SOURCE_DIR/utopia/build_cpu
cd $UTOPIA_SOURCE_DIR/utopia/build_cpu &&  \
     cmake .. -DUTOPIA_DEPENDENCIES_DIR=$INSTALL_DIR \
              -DCMAKE_INSTALL_PREFIX=$UTOPIA_DIR \
              -DUTOPIA_ENABLE_YAML_CPP=ON -Dyaml-cpp_DIR=$YAML_CPP_DIR/share/cmake/yaml-cpp/ \
              -DUTOPIA_ENABLE_TRACE=ON \
              -DCMAKE_BUILD_TYPE=Release  \
              && make -j16 && make install

## FEM

cd $UTOPIA_SOURCE_DIR/utopia_fe && mkdir $UTOPIA_SOURCE_DIR/utopia_fe/build_cpu
cd $UTOPIA_SOURCE_DIR/utopia_fe/build_cpu &&  \
   cmake .. -DUTOPIA_DEPENDENCIES_DIR=$INSTALL_DIR \
             -DCMAKE_INSTALL_PREFIX=$UTOPIA_FE_DIR \
             -DUTOPIA_ENABLE_LIBMESH=OFF \
             -DUTOPIA_ENABLE_MOONOLITH=ON \
             -DUTOPIA_ENABLE_INTREPID2=ON \
             -DUTOPIA_ENABLE_STK=ON         \
             -DUTOPIA_ENABLE_MARS=ON  \
             -DUTOPIA_ENABLE_MARS_VTK=OFF \
             -DUTOPIA_ENABLE_PETSC=ON  \
             -DIntrepid2_DIR=$INTREPID2_DIR \
             -DCMAKE_BUILD_TYPE=Release  \
    && make -j16 complete && make install


cd $USER_WORK_DIR
