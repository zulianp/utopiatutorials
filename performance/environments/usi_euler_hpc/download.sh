#!/bin/bash
source init.sh

########################## MARS ##########################################

cd $UTOPIA_PERFORMANCE_ROOT_DIR
git clone https://zulianp@bitbucket.org/zulianp/mars.git $MARS_SOURCE_DIR
cd $MARS_SOURCE_DIR && git pull
cd $MARS_SOURCE_DIR && git checkout development
# cd $MARS_SOURCE_DIR && git checkout side_map_fix
cd $MARS_SOURCE_DIR && git submodule update --init --recursive
cd $MARS_SOURCE_DIR && git pull

########################## YAML ##########################################

cd $UTOPIA_PERFORMANCE_ROOT_DIR
git clone https://github.com/jbeder/yaml-cpp.git $YAML_CPP_SOURCE_DIR

########################## MOONOLITH ##########################################

cd $UTOPIA_PERFORMANCE_ROOT_DIR
git clone https://zulianp@bitbucket.org/zulianp/par_moonolith.git $MOONOLITH_SOURCE_DIR
cd $MOONOLITH_SOURCE_DIR
cd $MOONOLITH_SOURCE_DIR && git checkout development
cd $MOONOLITH_SOURCE_DIR && git pull

########################## UTOPIA ##########################################

cd $UTOPIA_PERFORMANCE_ROOT_DIR
git clone https://bitbucket.org/zulianp/utopia.git $UTOPIA_SOURCE_DIR
cd $UTOPIA_SOURCE_DIR
cd $UTOPIA_SOURCE_DIR && git checkout development
cd $UTOPIA_SOURCE_DIR && git submodule update --init --recursive
cd $UTOPIA_SOURCE_DIR && git pull

cd $UTOPIA_PERFORMANCE_ROOT_DIR
git clone https://github.com/trilinos/Trilinos.git $TRILINOS_SOURCE_DIR
cd $TRILINOS_SOURCE_DIR && git pull

