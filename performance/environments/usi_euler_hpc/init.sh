#!/bin/bash

export UTOPIA_PERFORMANCE_ROOT_DIR=$PWD
export CASES_DIR=$PWD/../cases
export DOWNLOAD_DIR=$UTOPIA_PERFORMANCE_ROOT_DIR/downloads

# Download directories
export MARS_SOURCE_DIR=$DOWNLOAD_DIR/mars
export YAML_CPP_SOURCE_DIR=$DOWNLOAD_DIR/yaml-cpp
export MOONOLITH_SOURCE_DIR=$DOWNLOAD_DIR/par_moonolith
export UTOPIA_SOURCE_DIR=$DOWNLOAD_DIR/utopia
export TRILINOS_SOURCE_DIR=$DOWNLOAD_DIR/Trilinos

export GPU_DIR=$PWD/gpu
export MC_DIR=$PWD/mc
