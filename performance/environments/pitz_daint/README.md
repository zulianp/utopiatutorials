# For compiling everything

Execute following command

`source install.sh`

# For running sessions

Execute following command

`source init.sh`

For gpu

`cd gpu && source run.sh`


For multicore-cpu

`cd mc && source run.sh`

Note that the `run.sh` scripts are WIP, you can use them as examples for your runs.