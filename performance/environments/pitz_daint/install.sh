#!/bin/bash

source download.sh

cd gpu
source install_gpu.sh

cd $UTOPIA_PERFORMANCE_ROOT_DIR

cd mc

source install_cpu.sh

cd $UTOPIA_PERFORMANCE_ROOT_DIR
