#!/bin/bash
source init.sh

########################## MARS ##########################################

mkdir $MARS_SOURCE_DIR/build_gpu
cd $MARS_SOURCE_DIR/build_gpu && cmake .. \
-DCMAKE_VERBOSE_MAKEFILE=ON  \
-DCMAKE_BUILD_TYPE=Release  \
-DMARS_ENABLE_CUDA=ON  \
-DMARS_ENABLE_CUDAUVM=ON \
-DCMAKE_CXX_COMPILER=$NVCC_WRAPPER \
-DCMAKE_INSTALL_PREFIX=$MARS_DIR \
&& make -j16 && make install

########################## YAML ##########################################

mkdir $YAML_CPP_SOURCE_DIR/build_gpu
cd $YAML_CPP_SOURCE_DIR/build_gpu && cmake .. \
    -DCMAKE_INSTALL_PREFIX=$YAML_CPP_DIR \
    && make -j16 && make install

########################## MOONOLITH ##########################################

mkdir $MOONOLITH_SOURCE_DIR/build_gpu
cd $MOONOLITH_SOURCE_DIR/build_gpu && cmake .. -DCMAKE_INSTALL_PREFIX=$MOONOLITH_DIR  -DCMAKE_BUILD_TYPE=Release \
 && make -j && make install

 ########################## UTOPIA ##########################################

## ALGEBRA

cd $UTOPIA_SOURCE_DIR/utopia && mkdir $UTOPIA_SOURCE_DIR/utopia/build_gpu
cd $UTOPIA_SOURCE_DIR/utopia/build_gpu &&  \
     cmake .. -DUTOPIA_DEPENDENCIES_DIR=$INSTALL_DIR \
              -DCMAKE_INSTALL_PREFIX=$UTOPIA_DIR \
              -DUTOPIA_ENABLE_YAML_CPP=ON -Dyaml-cpp_DIR=$YAML_CPP_DIR/share/cmake/yaml-cpp/ \
              -DUTOPIA_ENABLE_TRACE=ON \
              -DCMAKE_BUILD_TYPE=Release  \
              -DCMAKE_CXX_COMPILER=$NVCC_WRAPPER \
              && make -j16 && make install

## FEM

cd $UTOPIA_SOURCE_DIR/utopia_fe && mkdir $UTOPIA_SOURCE_DIR/utopia_fe/build_gpu
cd  $UTOPIA_SOURCE_DIR/utopia_fe/build_gpu &&  \
   cmake .. -DUTOPIA_DEPENDENCIES_DIR=$INSTALL_DIR \
             -DCMAKE_INSTALL_PREFIX=$UTOPIA_FE_DIR \
             -DUTOPIA_ENABLE_LIBMESH=OFF \
             -DUTOPIA_ENABLE_MOONOLITH=ON \
             -DUTOPIA_ENABLE_INTREPID2=ON \
             -DUTOPIA_ENABLE_STK=ON         \
             -DUTOPIA_ENABLE_MARS=ON  \
             -DUTOPIA_ENABLE_MARS_VTK=OFF \
             -DUTOPIA_ENABLE_PETSC=ON  \
             -DIntrepid2_DIR=$INTREPID2_DIR \
             -DCMAKE_BUILD_TYPE=Release  \
             -DCMAKE_CXX_COMPILER=$NVCC_WRAPPER \
    && make -j16 complete && make install


cd $USER_WORK_DIR
