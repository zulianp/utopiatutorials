#!/bin/bash
export USER_WORK_DIR=$PWD
export CASES_DIR=$PWD/../../cases

# Sanitize
export LD_LIBRARY_PATH=""

# load env
source $APPS/UES/anfink/gpu/environment

export INTREPID2_DIR=$TRILINOS_DIR/lib/cmake/Intrepid2
export LD_LIBRARY_PATH=$LIBMESH_DIR/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$PETSC_DIR/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$TRILINOS_DIR/lib:$LD_LIBRARY_PATH

export MPICH_RDMA_ENABLED_CUDA=1; export CUDA_LAUNCH_BLOCKING=1
export CUDA_LAUNCH_BLOCKING=1; export MPICH_RDMA_ENABLED_CUDA=1
export MPICH_GNI_LMT_PATH=disabled

# Installation directories
export INSTALL_DIR=$USER_WORK_DIR/installations/

export YAML_CPP_DIR=$INSTALL_DIR/yaml-cpp
export MARS_DIR=$INSTALL_DIR/mars
export MOONOLITH_DIR=$INSTALL_DIR/par_moonolith
export UTOPIA_DIR=$INSTALL_DIR/utopia
export UTOPIA_FE_DIR=$INSTALL_DIR/utopia_fe

export UTOPIA_FE_EXEC_GPU=$UTOPIA_SOURCE_DIR/utopia_fe/build_gpu/utopia_fe_exec
export UTOPIA_FE_TEST_GPU=$UTOPIA_SOURCE_DIR/utopia_fe/build_gpu/utopia_fe_test

# Usefull binaries
export NVCC_WRAPPER=/apps/daint/UES/anfink/gpu/trilinos/bin/nvcc_wrapper

# export KOKKOS_PROFILE_LIBRARY=$HOME/installations/kokkos-tools/kp_kernel_timer.so