#!/bin/bash

export EXPERIMENT_DIR=$PWD
cd ..
source init.sh
export UTOPIA_FE_EXEC_GPU=$UTOPIA_SOURCE_DIR/utopia_fe/build_gpu/utopia_fe_exec
export UTOPIA_FE_EXEC_MC=$UTOPIA_SOURCE_DIR/utopia_fe/build_cpu/utopia_fe_exec

cd $EXPERIMENT_DIR

pwd

python3 launch_experiments.py