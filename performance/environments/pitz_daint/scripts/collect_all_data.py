import collect_scaling_data as csd

csd.create_csv("RUN_elasticity_gpu_mars", "elasticity_gpu_mars.csv")
csd.create_csv("RUN_elasticity_medium_gpu_mars", "elasticity_medium_gpu_mars.csv")
csd.create_csv("RUN_elasticity_large_gpu_mars", "elasticity_large_gpu_mars.csv")
# csd.create_csv("RUN_elasticity_mc_mars", "elasticity_mc_mars.csv")