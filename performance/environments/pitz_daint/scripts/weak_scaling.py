
import os
import subprocess
import math

# Note that constraints is always gpu for the moment! #SBATCH --constraint={device}

cases_dir='$CASES_DIR'

def run(case, device, backend, account, executable, num_nodes_array, tasks_per_node):
    input_file = cases_dir + '/' + case + '/' + case + '.yaml'
    base_dir = 'RUN_'+case+'_'+device+'_'+backend;

    print(base_dir)

    for nodes in num_nodes_array:
        dirname = f'{base_dir}/{nodes:03}nodes'
        jobfile = 'job.sbatch'

        cpus_per_task=1
        extra_opts=""
        if(device == "gpu"):
            tasks_per_node=1
            extra_opts="export OMP_PROC_BIND=false\nexport OMP_NUM_THREADS=1"
            extra_opts += "\nsource $APPS/UES/anfink/gpu/environment"
        else:
            cpus_per_task=tasks_per_node
            tasks_per_node=1
            extra_opts =  "\nsource $APPS/UES/anfink/cpu/environment"
            extra_opts += "\nexport OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK"
            extra_opts += "\nexport OMP_PROC_BIND=true"


        if not os.path.exists(dirname):
            os.makedirs(dirname)

        with open(os.path.join(dirname, jobfile), 'w') as scriptfile:
            scriptfile.write(f"""#!/bin/bash
#SBATCH --job-name=UFE_{case}_{device}_{backend}
#SBATCH --output=output_%j.out
#SBATCH --time=00:10:00
#SBATCH --nodes={nodes}
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node={tasks_per_node}
#SBATCH --cpus-per-task={cpus_per_task}
#SBATCH --constraint=gpu
#SBATCH --account={account}

{extra_opts}

srun {executable} --verbose -backend {backend} @file {input_file}
""")

        subprocess.call(['sbatch', jobfile], cwd=dirname)
