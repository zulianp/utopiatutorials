import weak_scaling as ws
import os

gpu_exec = os.environ.get('UTOPIA_FE_EXEC_GPU')
mc_exec = os.environ.get('UTOPIA_FE_EXEC_MC')

if(not mc_exec):
    print("ERROR: Environment variable UTOPIA_FE_EXEC_MC must be defined! See lauch_experiments.sh")
    exit()

if(not gpu_exec):
    print("ERROR: Environment variable UTOPIA_FE_EXEC_GPU must be defined! See lauch_experiments.sh")
    exit()


# nodes=(1, 2, 3)
small_nodes=(2,4,8,16)
medium_nodes=(16, 32) #, 64, 128)
large_nodes=(256, 512)

# small
# ws.run('elasticity', 'gpu', 'mars', 'c24', gpu_exec, small_nodes, 1)
# ws.run('elasticity', 'mc', 'mars', 'c24', mc_exec, small_nodes, 12)

# medium
# ws.run('elasticity_medium', 'gpu', 'mars', 'c24', gpu_exec, medium_nodes, 1)

# large
ws.run('elasticity_large', 'gpu', 'mars', 'c24', gpu_exec, large_nodes, 1)
