import csv
import glob


# measure_name = total|count|mean
def create_csv(folder, csv_output_file, measure_name="total"):
    methods = [
    "FunctionSpace::apply_constraints(m, diag_value)",
    "FunctionSpace::apply_constraints(v)",
    "FunctionSpace::apply_zero_constraints(vec)",
    "FunctionSpace::create_matrix(m)",
    "FunctionSpace::init(mesh)",
    "OmniAssembler::assemble(x,mat,vec)",
    "Linear solve",
    "main"
    ]

    check_methods = set(methods)

    subs_name = {}
    subs_name["main"] = "Total"
    subs_name["FunctionSpace::init(mesh)"] = "Mesh"
    subs_name["OmniAssembler::assemble(x,mat,vec)"] = "Assembly"
    subs_name["FunctionSpace::apply_constraints(m, diag_value)"] = "Constrain matrix"
    subs_name["FunctionSpace::apply_constraints(v)"] = "Constrain vector"
    subs_name["FunctionSpace::apply_zero_constraints(vec)"] = "Zero constrain vector"
    subs_name["FunctionSpace::create_matrix(m)"] = "Create matrix"
    subs_name["Linear solve"] = "CG steps"

    allData = []
    for filename in sorted(glob.glob(folder+'/[0-9]*nodes/summary.*.csv')):
        print(filename)

        start=len(folder)+1
        end=start+3
        numNodes = int(filename[start:end])
        print(f'numNodes={numNodes}')

        with open(filename) as f:

            allRows = [(r[0], r[2], r[3], r[5]) for r in csv.reader(f, delimiter=';')]
            assert(allRows[0][0]=='Class')
            assert(allRows[0][1]=='Total time spent (s)')
            assert(allRows[0][2]=='Count')
            assert(allRows[0][3]=='Mean time (s)')

            selectedRows = {}
            for r in allRows[1:]:
                if r[0] in check_methods:
                    selectedRows[subs_name[r[0]]] = dict(mean=float(r[3]), count=int(r[2]), total=float(r[1]))

            allData.append({'numNodes': numNodes, 'data': selectedRows})

    csv_out = open(csv_output_file, 'w')
    csv_out.write('Nodes,')
    csv_out.write(','.join([f'{subs_name[k]}' for k in methods]))

    for d in allData:
        csv_out.write(f'\n{d["numNodes"]}')

        for k in methods:
            v = d['data'][subs_name[k]]

            csv_out.write(f',{v[measure_name]}')


