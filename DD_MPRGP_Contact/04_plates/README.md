## Set-up vs result
 
Content (check and modify paths and parameters)

- `plates.yaml` Simulation input file
- `plates.jou` CubIt/Trelis script for generating the mesh
- `run.sh`  Script for running simulation on personal computer

![Set-up](plates.png)

