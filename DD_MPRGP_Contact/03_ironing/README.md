## Set-up vs result
 
Content (check and modify paths and parameters)

- `ironing.yaml` Simulation input file
- `ironing.jou` CubIt/Trelis script for generating the mesh
- `run.sh`  Script for running simulation on personal computer

![Set-up](ironing.png)

