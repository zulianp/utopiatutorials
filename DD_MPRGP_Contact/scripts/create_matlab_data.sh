#!/usr/bin/env bash

set -e

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

export PATH=/Applications/MATLAB_R2022a.app/bin/:$PATH

if (($# != 1))
then
	printf "usage: $0 <sqp_root>\n" 1>&2
	exit -1
fi

matlab_script=$SCRIPTPATH/load_sqp.m 
here=$PWD
dir=$1

cd $dir
sqps=`ls`

echo $sqps
current_dir=`pwd`
for subdir in ${sqps[@]}
do
	echo "At \"$current_dir/$subdir\""
	cd $current_dir/$subdir
	cp $matlab_script ./

	echo "Running matlab..."
	matlab -nodisplay -nosplash -nodesktop -batch "run('load_sqp.m'); exit;"
	cd -
done


