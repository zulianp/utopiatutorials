#!/usr/bin/env bash
set -e
set -x

rm -f summary*.csv
rm -f obs_out.e*

echo "DUMP" > dump.txt

procs=8
mesh=hertzian.e

$TRILINOS_DIR/bin/decomp -p $procs $mesh >> dump.txt

mpiexec -np $procs $UTOPIA_FE_EXEC --verbose @file hertzian.yaml

$TRILINOS_DIR/bin/epu -auto  hertzian_out.e."$procs".0 >> dump.txt
