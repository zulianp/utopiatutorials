#!/usr/bin/env bash

set -e

young_modulus=10
poisson_ratio=0.3

export PATH=$CODE_DIR/sfem/workflows/mech:$PATH

db=hertzian_ref2_out.e 

# mu=1e3; 
mu=`python3 -c  'young_modulus='$young_modulus';  poisson_ratio='$poisson_ratio'; print(young_modulus / (2 * (1 + poisson_ratio)))'`
lambda=`python3 -c 'poisson_ratio='$poisson_ratio'; mu='$mu'; print(2.0 * (mu * poisson_ratio) / (1 - 2 * poisson_ratio ))'`

workspace=dump

[ -d "$workspace" ] && rm -rf $workspace

ncdump -h $db

stress.sh $db $workspace $mu $lambda
# ./strain.sh $db $workspace
# ./principal_strains.sh $db $workspace
# ./principal_stresses.sh $db $workspace $mu $lambda