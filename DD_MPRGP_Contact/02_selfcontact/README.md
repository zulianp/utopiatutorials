## Set-up vs result
 
Content (check and modify paths and parameters)

- `selfcontact.yaml` Simulation input file
- `selfcontact.jou` CubIt/Trelis script for generating the mesh
- `run.sh`  Script for running simulation on personal computer

![Set-up](selfcontact.png)

