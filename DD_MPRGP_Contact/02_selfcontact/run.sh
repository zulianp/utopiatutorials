#!/usr/bin/env bash
set -e
set -x

rm -f summary*.csv
rm -f *_out.e*

echo "DUMP" > dump.txt

procs=8
mesh=selfcontact.e

$TRILINOS_DIR/bin/decomp -p $procs $mesh >> dump.txt

mpiexec -np $procs $UTOPIA_FE_EXEC --verbose @file selfcontact.yaml

$TRILINOS_DIR/bin/epu -auto  selfcontact_out.e."$procs".0 >> dump.txt
