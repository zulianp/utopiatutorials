## Set-up vs result
 
Content (check and modify paths and parameters)

- `ring_sphere.yaml` Simulation input file
- `ring_sphere.jou` CubIt/Trelis script for generating the mesh
- `run.sh`  Script for running simulation on personal computer

![Set-up](ring_sphere.png)

