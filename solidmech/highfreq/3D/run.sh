#!/usr/bin/env bash
set -e

# Executables
which $TRILINOS_DIR/bin/decomp $UTOPIA_FE_EXEC

$TRILINOS_DIR/bin/decomp -p 8 mesh.e
mpiexec -np 8 $UTOPIA_FE_EXEC @file highfreq3D.yaml
