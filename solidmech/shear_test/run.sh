#!/usr/bin/env bash

set -e
set -x

$UTOPIA_FE_EXEC @file shear_test.yaml
echo 'ls -lah' && ls -lah