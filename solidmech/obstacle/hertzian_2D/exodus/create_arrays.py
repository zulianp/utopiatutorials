#!/usr/bin/env python3

import numpy as np

# t=np.array([0, 4.0001], dtype=np.float32); 
# t.tofile('time.float32.raw')

# s=np.array([0, -0.01], dtype=np.float32); 
# s.tofile('signal.float32.raw')


t=np.linspace(0, 40.0001, num=100, dtype=np.float32); 
t.tofile('time.float32.raw')

s=0.01 * np.cos(np.pi/2 + 4 * np.pi * t/np.max(t))
s.tofile('signal.float32.raw')

print(s)
