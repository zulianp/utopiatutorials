#!/usr/bin/env bash
set -e
set -x

rm -f summary*.csv
rm -f obs_out.e*

echo "DUMP" > dump.txt

$TRILINOS_DIR/bin/perceptX --refine --input_mesh=mesh.e   --output_mesh=mesh_1.e >> dump.txt
$TRILINOS_DIR/bin/perceptX --refine --input_mesh=mesh_1.e --output_mesh=mesh_2.e >> dump.txt
$TRILINOS_DIR/bin/perceptX --refine --input_mesh=mesh_2.e --output_mesh=mesh_3.e >> dump.txt
# $TRILINOS_DIR/bin/perceptX --refine --input_mesh=mesh_3.e --output_mesh=mesh_4.e >> dump.txt
# $TRILINOS_DIR/bin/perceptX --refine --input_mesh=mesh_3.e --output_mesh=mesh_5.e >> dump.txt

mv mesh_3.e mesh_ref.e
# mv mesh_5.e mesh_ref.e

$TRILINOS_DIR/bin/decomp -p 8 mesh_ref.e >> dump.txt

mpiexec -np 8 $UTOPIA_FE_EXEC --verbose @file hertzian_2D.yaml

$TRILINOS_DIR/bin/epu -auto obs_out.e.8.0 >> dump.txt

rm mesh_*.e*

# MPRGP::solve(...);0;11.2253;6;0;1.87088
# TaoSolver::solve;0;7.69936;6;0;1.28323
# MPRGP::solve(...);0;3.8552;6;0;0.642534