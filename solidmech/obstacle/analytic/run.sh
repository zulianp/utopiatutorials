#!/usr/bin/env bash

set -e
set -x

mpiexec -np 8 $UTOPIA_FE_EXEC --verbose @file analytic.yaml
# mpiexec -np 2 /Users/zulianp/Desktop/code/utopia/utopia_fe/build_intrepid2/utopia_fe_exec --verbose @file analytic.yaml -on_error_attach_debugger lldb