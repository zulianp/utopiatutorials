#!/usr/bin/env bash

export UTOPIA_FE_INSTALL_DIR=$INSTALL_DIR/utopia_fe

mesh=/Users/patrickzulian/Desktop/cloud/owncloud_HSLU/Patrick/2023/Cases/FP70/solid.exo
obstacle=/Users/patrickzulian/Desktop/cloud/owncloud_HSLU/Patrick/2023/Cases/FP70/fluid.exo
hmax=0.00005
margin=0.0002

./sdf_obstacle.sh $mesh $obstacle $hmax $margin