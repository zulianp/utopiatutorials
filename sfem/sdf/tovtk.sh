#!/usr/bin/env bash

set -e

SFEM_DIR=$CODE_DIR/sfem
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/0 dbg0.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/1 dbg1.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/2 dbg2.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/3 dbg3.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/4 dbg4.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/5 dbg5.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/6 dbg6.vtk
$SFEM_DIR/python/mesh/raw_to_db.py dbg_mesh0/7 dbg7.vtk

