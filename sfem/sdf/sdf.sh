#!/usr/bin/env bash

set -e

if [[ $# -lt "4" ]]
then
	printf "Passed $# args\n" 1>&2
	printf "usage: $0 <mesh.e> <obstacle.e> <hmax> <margin>\n" 1>&2
	exit -1
fi

if [[ -z "$UTOPIA_FE_INSTALL_DIR" ]]
then
	echo "Define UTOPIA_FE_INSTALL_DIR"
	exit -1
fi

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# export UTOPIA_FE_EXEC=$CODE_DIR/utopia/utopia_fe/build_debug/utopia_fe_exec
export UTOPIA_FE_EXEC=$CODE_DIR/utopia/utopia_fe/build/utopia_fe_exec
PATH=$CODE_DIR/sfem:$PATH
PATH=$UTOPIA_FE_INSTALL_DIR/scripts:$PATH

set -x

mesh=$1
obstacle=$2
hmax=$3
margin=$4

mkdir -p db_sfem
mkdir -p db_sfem/mesh
mkdir -p db_sfem/obstacle
mkdir -p db_vtk
mkdir -p db_sdf

sdf=db_sdf/sdf.float32.raw
sdf_yaml=db_sdf/metadata_sdf.float32.yml

# Covert meshes to sfem format
db_to_raw.py $mesh db_sfem/mesh
db_to_raw.py $obstacle db_sfem/obstacle

# Skin meshes
skin db_sfem/mesh db_sfem/mesh/surface
skin db_sfem/obstacle db_sfem/obstacle/surface

# Have VTK meshes of the surfaces for dbg
raw_to_db.py db_sfem/mesh/surface db_vtk/surf_mesh.vtk
raw_to_db.py db_sfem/obstacle/surface db_vtk/surf_obstacle.vtk

mesh_to_sdf.py db_sfem/obstacle/surface $sdf --hmax=$hmax --margin=$margin --scale_box=1.1 --box_from_mesh=db_sfem/mesh/surface
raw_to_xdmf.py $sdf

rm -f sim.yaml temp.yaml
( echo "cat <<EOF >sim.yaml";
  cat $SCRIPTPATH/input.tpl.yaml;
  echo "EOF";
) >temp.yaml
. temp.yaml


# $UTOPIA_FE_EXEC --verbose @file sim.yaml #--trace_root_log_regions
# mv sdf.e oracle.e

LAUNCH="mpiexec -np 8"
$LAUNCH $UTOPIA_FE_EXEC --verbose @file sim.yaml --trace_root_log_regions
