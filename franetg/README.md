# FraNetG #

Here we find examples on how to run phase-filed fracture simulations with Utopia.


## Table of content ##

- **2D** 
	- **isotropic_tension** (Simulation of a fracture propagation due to tension using the trust region method)
	- **asphalt_tension** (Tension test of asphalt specimen using the RMTR -- Recursive multilevel trust-region -- method)
