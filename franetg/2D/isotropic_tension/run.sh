#!/usr/bin/env bash

set -e
set -x



if [ -z "$UTOPIA_EXEC" ]; then 
	echo "Define UTOPIA_EXEC=/path/to/executable/utopia_exec"
else
	mpiexec -np 8 $UTOPIA_EXEC @file input.yaml
fi
