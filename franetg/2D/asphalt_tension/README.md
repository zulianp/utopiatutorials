# Tension test of asphalt specimen #

The set-up of the experiment is explained [HERE](https://link.springer.com/article/10.1007/s42514-021-00069-6), in the **Validation** section.

## Run the example ##
Define the envrinment variable `UTOPIA_EXEC` to point to the `utopia_exec` executable. For instance
```bash
	export UTOPIA_EXEC=~/code/utopia/utopia/build/utopia_exec
```

Note that for (re-)compiling the utopia executable your require to type `make -j4 utopia_exec`.

For running the bash script type


```bash
	./run.sh
```

If you do not have permissions to run it, then write

```bash
chmod 770 run.sh
```

## Code ##

### The driver ###

The command `AsphaltTension2d_rmtr` can be found
[HERE](https://bitbucket.org/zulianp/utopia/src/development/utopia/apps/PF_frac/utopia_isotropic_phase_field_app.cpp).

This example uses the RMTR method for solving the equation system. In this reduced code sample we find the main entry point.

```cpp

// The app is a C++ function that takes an `Input` object
static void AsphaltTension2d_rmtr(Input &in) {
	// The geometry is 2D
    static const int Dim = 2;

    // Hence we have 1 (phase-field) + 2 (displacement) = 3 variables
    static const int NVars = 1 + Dim;

    // We define the mesh as a structured grid
    using Mesh = utopia::PetscStructuredGrid<Dim>;

    // We define the elements as uniform quadrilateral with bilinear basis functions
    using Elem = utopia::PetscUniformQuad4;

    // We define the type of finite element space of our problem
    using FunctionSpace = utopia::FunctionSpace<Mesh, NVars, Elem>;

    FunctionSpace space;

    // We read the specification of our function space from our input file
    space.read(in);


    // MLIncrementalLoading applies a pseudo time-stepping to the problem, so that 
    // the displacement boundary conditions are applied incrementally
    MLIncrementalLoading<FunctionSpace,							// The function space used to discretize the problem
                         FractureModel<FunctionSpace>,			// The phase-field model/assembler implementation
                         AsphaltTension2D<FunctionSpace>,		// The boundary conditions for this problem
                         AsphaltTension<FunctionSpace>>			// The initial conditions for this problem
        time_stepper(space); // The object is constructed base on the function space.

    // The input file is read also for the incremental loading algorithm
    time_stepper.read(in);

    // Finally we run the whole simulation!
    time_stepper.run();
}

// Here we register the function as an app
// In the input file we can refer to this app with the line
// app: AsphaltTension2d_rmtr
UTOPIA_REGISTER_APP(AsphaltTension2d_rmtr);
```

## The fracture model ##

We have two versions of the isotropic phase-field fracture model. The `Vc` verions exploits explicit SIMD vectorization providing a 2.7x speed-up on processors supporting `avx2` instructions. If not available we fallback to the non-vectorized version.

```cpp
    template <class FunctionSpace>
#ifdef UTOPIA_WITH_VC
    using FractureModel = utopia::VcIsotropicPhaseFieldForBrittleFractures<FunctionSpace>;
#else
    using FractureModel = utopia::IsotropicPhaseFieldForBrittleFractures<FunctionSpace>;
#endif
```
The source-code of the model can be found [HERE](https://bitbucket.org/zulianp/utopia/src/development/utopia/apps/PF_frac/utopia_IsotropicPhaseField.hpp)

Every model needs for the implementation of the following methods:

- **bool value(const Vector &x, Scalar &val) const** This method evaluates the objective function related to the total energy (elastic + fracture) from the current solution `x` and writes it into the `val` variable.
- **bool gradient(const Vector &x, Vector &g) const** This method evaluates the gradient of the energy and writes it into the vector `g`
- **bool hessian(const Vector &x, Matrix &H) const** This method evaluates the hessian of the energy and writes it into the matrix `H`
- **bool elastic_energy(const Vector &x, Scalar &val) const override** This method evaluates the elastic energy for analysis
- **bool fracture_energy(const Vector &x, Scalar &val) const override** This method evaluates the fracture energy for analysis

The code makes use of functor objects which allow to hide certain details. However, they are not required if the user wants to write the routines from scratch.

## The boundary conditions ##

In this experiment the mapping is `0` for phase-field varible, `1` and `2` for the displacement variables in the `x` and `y` coordinate, respectively. 
We impose dirichlet boundary conditions to the component `2` of the vector problem.
The magnitude of the displacement in `y` can be controlled from the input file with the `disp_y` entry.

```cpp
template <class FunctionSpace>
class AsphaltTension2D : public BCSetup<FunctionSpace> {
public:
    using Scalar = typename FunctionSpace::Scalar;
    using Vector = typename FunctionSpace::Vector;

    AsphaltTension2D(FunctionSpace &space, const Scalar &disp_y = 1.0)
        : BCSetup<FunctionSpace>(space), disp_y_(disp_y) {}

    void read(Input &in) override { in.get("disp_y", disp_y_); }

    void emplace_time_dependent_BC(const Scalar &time) override {
        using Point = typename FunctionSpace::Point;
        this->space_.reset_bc();

        this->space_.emplace_dirichlet_condition(
            SideSet::bottom(),
            UTOPIA_LAMBDA(const Point &)->Scalar { return 0.0; },
            1  // disp_x
        );

        this->space_.emplace_dirichlet_condition(
            SideSet::bottom(),
            UTOPIA_LAMBDA(const Point &)->Scalar { return 0.0; },
            2  // disp_y
        );

        this->space_.emplace_dirichlet_condition(
            SideSet::top(),
            UTOPIA_LAMBDA(const Point &)->Scalar { return disp_y_ * time; },
            2  // disp_y
        );
    }

private:
    Scalar disp_y_;
};

```

## The initial conditions and geometrical set-up##

The initial condition subtype enables the user to set-up the initial state of the phase-field variable. 
In this experiment we simulate the propagation of two rectangle-shaped fractures embedded in rectanguar background matrix.

```cpp
template <class FunctionSpace>
class AsphaltTension : public InitialCondition<FunctionSpace> {
public:
    // using Comm           = typename FunctionSpace::Comm;
    using Mesh = typename FunctionSpace::Mesh;
    using Elem = typename FunctionSpace::Shape;
    using ElemView = typename FunctionSpace::ViewDevice::Elem;
    using SizeType = typename FunctionSpace::SizeType;
    using Scalar = typename FunctionSpace::Scalar;
    using Dev = typename FunctionSpace::Device;
    using Point = typename FunctionSpace::Point;
    using ElemViewScalar = typename utopia::FunctionSpace<Mesh, 1, Elem>::ViewDevice::Elem;
    static const int NNodes = Elem::NNodes;

    AsphaltTension(FunctionSpace &space, const SizeType &PF_component)
        : InitialCondition<FunctionSpace>(space), PF_component_(PF_component), pressure0_(1.0) {}

    void init(PetscVector &x) override {
        using CoeffVector = utopia::StaticVector<Scalar, NNodes>;
        auto C = this->space_.subspace(PF_component_);

        auto width = 3.0 * this->space_.mesh().min_spacing();

        std::vector<Rectangle<Scalar>> rectangles;

        Point2D<Scalar> A{};
        A.x = 12.00;
        A.y = 21.50;
        rectangles.push_back(Rectangle<Scalar>(A, -3.5355, width, 45));

        Point2D<Scalar> B{};
        B.x = 15;
        B.y = 20.00;
        rectangles.push_back(Rectangle<Scalar>(B, 5.000, width, 0.0));

        auto sampler = utopia::sampler(C, [&rectangles](const Point &x) -> Scalar {
            for (std::size_t r = 0; r < rectangles.size(); r++) {
                if (rectangles[r].belongs_to_rectangle(x[0], x[1])) return 1.0;
            }
            return 0.0;
        });

        {
            auto C_view = C.view_device();
            auto sampler_view = sampler.view_device();
            auto x_view = this->space_.assembly_view_device(x);

            Dev::parallel_for(
                this->space_.element_range(), UTOPIA_LAMBDA(const SizeType &i) {
                    ElemViewScalar e;
                    C_view.elem(i, e);

                    CoeffVector s;
                    sampler_view.assemble(e, s);
                    C_view.set_vector(e, s, x_view);
                });
        }
    }

private:
    SizeType PF_component_;
    SizeType num_fracs_;
    Scalar pressure0_;
};

```
