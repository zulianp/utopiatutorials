#!/usr/bin/env bash

set -e
set -x

if [ -d "output" ] 
then
    echo "Warning: directory output exists. Remove content?"
    read -p "(y/n):" user_input

    if [ "$user_input" == "y" ] 
    then
    	echo "Removing content in output!"
    	rm output/*
   	fi
else
    echo "Error: Directory /output does not exists."
fi

if [ -z "$UTOPIA_EXEC" ]; then 
	echo "Define UTOPIA_EXEC=/path/to/executable/utopia_exec"
else
	mpiexec -np 8 $UTOPIA_EXEC @file input.yaml
fi
