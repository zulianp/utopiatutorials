SetFactory("OpenCASCADE");

aperture = 0.0005;
// aperture = 0.01;
x1 = 0.5;
y1 = 0.5;

x2 = 0.75;
y2 = 0.5;

x3 = 1;
y3 = 0.75;

x4 = 0.5;
y4 = 1;

x5 = 0.5;
y5 = 0.75;

x6 = 0.625;
y6 = 0.625;

//+ Big Fractures
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {1, 0, 0, 1.0};
//+
Point(3) = {1, 1, 0, 1.0};
//+
Point(4) = {0, 1, 0, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Point(5) = {x1 - aperture, 0, 0, 1.0};
//+
Point(6) = {x1 + aperture, 0, 0, 1.0};
//+
Point(7) = {x1 + aperture, 1, 0, 1.0};
//+
Point(8) = {x1 - aperture, 1, 0, 1.0};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 5};
//+
Curve Loop(2) = {5, 6, 7, 8};
//+
Point(9) = {0, y1 - aperture, 0, 1.0};
//+
Point(10) = {0, y1 + aperture, 0, 1.0};
//+
Point(11) = {1, y1 + aperture, 0, 1.0};
//+
Point(12) = {1, y1 - aperture, 0, 1.0};
//+
Line(9) = {9, 10};
//+
Line(10) = {10, 11};
//+
Line(11) = {11, 12};
//+
Line(12) = {12, 9};
//+
Curve Loop(3) = {9, 10, 11, 12};

//+ Medium Fractures
Point(13) = {x2 - aperture, y2, 0, 1.0};
Point(14) = {x2 + aperture, y2, 0, 1.0};
Point(15) = {x2 + aperture, y4, 0, 1.0};
Point(16) = {x2 - aperture, y4, 0, 1.0};
Line(13) = {13, 14};
Line(14) = {14, 15};
Line(15) = {15, 16};
Line(16) = {16, 13};
Curve Loop(4) = {13, 14, 15, 16};

Point(17) = {x5, y3 - aperture, 0, 1.0};
Point(18) = {x5, y3 + aperture, 0, 1.0};
Point(19) = {x3, y3 + aperture, 0, 1.0};
Point(20) = {x3, y3 - aperture, 0, 1.0};
Line(17) = {17, 18};
Line(18) = {18, 19};
Line(19) = {19, 20};
Line(20) = {20, 17};
Curve Loop(5) = {17, 18, 19, 20};

//+ Small Fractures
Point(21) = {x6 - aperture, y1, 0, 1.0};
Point(22) = {x6 + aperture, y1, 0, 1.0};
Point(23) = {x6 + aperture, y3, 0, 1.0};
Point(24) = {x6 - aperture, y3, 0, 1.0};
Line(21) = {21, 22};
Line(22) = {22, 23};
Line(23) = {23, 24};
Line(24) = {24, 21};
Curve Loop(6) = {21, 22, 23, 24};

Point(25) = {x1, y6 - aperture, 0, 1.0};
Point(26) = {x1, y6 + aperture, 0, 1.0};
Point(27) = {x2, y6 + aperture, 0, 1.0};
Point(28) = {x2, y6 - aperture, 0, 1.0};
Line(25) = {25, 26};
Line(26) = {26, 27};
Line(27) = {27, 28};
Line(28) = {28, 25};
Curve Loop(7) = {25, 26, 27, 28};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};

//+
u() = BooleanUnion{ Surface{2}; Delete; }{ Surface{3}; Delete; };
u() = BooleanUnion{ Surface{u()}; Delete; }{ Surface{4}; Delete; };
u() = BooleanUnion{ Surface{u()}; Delete; }{ Surface{5}; Delete; };
u() = BooleanUnion{ Surface{u()}; Delete; }{ Surface{6}; Delete; };
u() = BooleanUnion{ Surface{u()}; Delete; }{ Surface{7}; Delete; };
d() = BooleanDifference{ Surface{1}; Delete; }{ Surface{u()}; };
//+
Physical Surface("matrix", 1000) ={u(), d()};
