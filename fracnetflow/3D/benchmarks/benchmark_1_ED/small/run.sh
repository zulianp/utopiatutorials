#!/usr/bin/env bash

set -e
set -x

$UTOPIA_FE_EXEC @file ../flow.yaml 
$UTOPIA_FE_EXEC @file ../transport.yaml