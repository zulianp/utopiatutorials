#!/bin/bash

N_PROCS=1
echo "Running: "
echo 'mpiexec -np '$N_PROCS' '$UTOPIA_FE_EXEC' @file ../flow.yaml'
echo ""
mpiexec -np $N_PROCS $UTOPIA_FE_EXEC @file ../flow.yaml -on_error_attach_debugger lldb
# && $UTOPIA_FE_EXEC @file ../transport.yaml