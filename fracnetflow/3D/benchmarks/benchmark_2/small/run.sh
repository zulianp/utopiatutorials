#!/usr/bin/env bash

if [[ -z "$UTOPIA_FE_EXEC" ]]
then
	echo "define UTOPIA_FE_EXEC"
	exit 1
fi

if [[ -z "$TRILINOS_DIR" ]]
then
	echo "define TRILINOS_DIR"
	exit 1
fi

export PATH=$TRILINOS_DIR/bin:$PATH

echo "Running: "
echo $UTOPIA_FE_EXEC' @file ../flow.yaml'
echo ""


$UTOPIA_FE_EXEC @file ../flow.yaml && $UTOPIA_FE_EXEC @file ../transport.yaml