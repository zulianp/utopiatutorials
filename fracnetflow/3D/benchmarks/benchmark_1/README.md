For running the different experiments go to the respetive folders, e.g. 
`cd medium`
and run the script, .e.g,
`source run.sh`

The scripts are written in YAML, hence utopia needs the following dependencies:

https://github.com/jbeder/yaml-cpp

In the utopia build folder type:
`cmake .. -DUTOPIA_ENABLE_YAML_CPP=ON -Dyaml-cpp_DIR=<PATH_TO_YAML_CMAKE_CONFIG_FILE>`

for instance
`cmake -DUTOPIA_ENABLE_YAML_CPP=ON -Dyaml-cpp_DIR=installations/yaml-cpp/share/cmake/yaml-cpp/`

Alternatively any online YAML to JSON converter can be used instead for having JSON scripts.

