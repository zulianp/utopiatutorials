#!/usr/bin/env bash

set -e
set -x

if [[ -z "$UTOPIA_FE_EXEC" ]]
then
	echo "define UTOPIA_FE_EXEC"
	exit 1
fi

if [[ -z "$TRILINOS_DIR" ]]
then
	echo "define TRILINOS_DIR"
	exit 1
fi

n_procs=8

$TRILINOS_DIR/bin/decomp -p $n_procs mesh_matrix.e
$TRILINOS_DIR/bin/decomp -p $n_procs mesh_fracture.e

mpiexec -np $n_procs $UTOPIA_FE_EXEC @file ../flow.yaml 

$TRILINOS_DIR/bin/epu -auto porous_matrix_out.e."$n_procs".0 > epu.log.txt
$TRILINOS_DIR/bin/epu -auto fracture_network_out.e."$n_procs".0 > epu.log.txt


# mpiexec -np $n_procs 
$UTOPIA_FE_EXEC @file ../transport.yaml