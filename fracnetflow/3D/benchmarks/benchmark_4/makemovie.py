# trace generated using paraview version 5.10.1
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 10

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active source.
porous_matrix_transport_oute1600 = GetActiveSource()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
porous_matrix_transport_oute1600Display = Show(porous_matrix_transport_oute1600, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
porous_matrix_transport_oute1600Display.Representation = 'Surface'
porous_matrix_transport_oute1600Display.ColorArrayName = [None, '']
porous_matrix_transport_oute1600Display.SelectTCoordArray = 'None'
porous_matrix_transport_oute1600Display.SelectNormalArray = 'None'
porous_matrix_transport_oute1600Display.SelectTangentArray = 'None'
porous_matrix_transport_oute1600Display.OSPRayScaleArray = 'concentration'
porous_matrix_transport_oute1600Display.OSPRayScaleFunction = 'PiecewiseFunction'
porous_matrix_transport_oute1600Display.SelectOrientationVectors = 'None'
porous_matrix_transport_oute1600Display.ScaleFactor = 140.00000000000009
porous_matrix_transport_oute1600Display.SelectScaleArray = 'None'
porous_matrix_transport_oute1600Display.GlyphType = 'Arrow'
porous_matrix_transport_oute1600Display.GlyphTableIndexArray = 'None'
porous_matrix_transport_oute1600Display.GaussianRadius = 7.0000000000000036
porous_matrix_transport_oute1600Display.SetScaleArray = ['POINTS', 'concentration']
porous_matrix_transport_oute1600Display.ScaleTransferFunction = 'PiecewiseFunction'
porous_matrix_transport_oute1600Display.OpacityArray = ['POINTS', 'concentration']
porous_matrix_transport_oute1600Display.OpacityTransferFunction = 'PiecewiseFunction'
porous_matrix_transport_oute1600Display.DataAxesGrid = 'GridAxesRepresentation'
porous_matrix_transport_oute1600Display.PolarAxes = 'PolarAxesRepresentation'
porous_matrix_transport_oute1600Display.ScalarOpacityUnitDistance = 4.391159830209163
porous_matrix_transport_oute1600Display.OpacityArrayName = ['POINTS', 'concentration']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
porous_matrix_transport_oute1600Display.ScaleTransferFunction.Points = [-6.383911013158657e-15, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
porous_matrix_transport_oute1600Display.OpacityTransferFunction.Points = [-6.383911013158657e-15, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera(False)

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(porous_matrix_transport_oute1600Display, ('POINTS', 'concentration'))

# rescale color and/or opacity maps used to include current data range
porous_matrix_transport_oute1600Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
porous_matrix_transport_oute1600Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'concentration'
concentrationLUT = GetColorTransferFunction('concentration')
concentrationLUT.RGBPoints = [-6.383911013158657e-15, 0.231373, 0.298039, 0.752941, 0.49999999999999684, 0.865003, 0.865003, 0.865003, 1.0, 0.705882, 0.0156863, 0.14902]
concentrationLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'concentration'
concentrationPWF = GetOpacityTransferFunction('concentration')
concentrationPWF.Points = [-6.383911013158657e-15, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
concentrationPWF.ScalarRangeInitialized = 1

# get color legend/bar for concentrationLUT in view renderView1
concentrationLUTColorBar = GetScalarBar(concentrationLUT, renderView1)
concentrationLUTColorBar.Title = 'concentration'
concentrationLUTColorBar.ComponentTitle = ''

# change scalar bar placement
concentrationLUTColorBar.WindowLocation = 'Any Location'
concentrationLUTColorBar.Position = [0.04448105436573313, 0.4582798459563543]
concentrationLUTColorBar.ScalarBarLength = 0.33000000000000007

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
concentrationLUT.ApplyPreset('erdc_blue2cyan_BW', True)

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
concentrationLUT.ApplyPreset('erdc_blue2cyan_BW', True)

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=porous_matrix_transport_oute1600)
contour1.ContourBy = ['POINTS', 'concentration']
contour1.Isosurfaces = [0.4999999999999968]
contour1.PointMergeMethod = 'Uniform Binning'

# Properties modified on contour1
contour1.Isosurfaces = [0.75, 0.7631578947368421, 0.7763157894736842, 0.7894736842105263, 0.8026315789473684, 0.8157894736842105, 0.8289473684210527, 0.8421052631578947, 0.8552631578947368, 0.868421052631579, 0.881578947368421, 0.8947368421052632, 0.9078947368421053, 0.9210526315789473, 0.9342105263157895, 0.9473684210526316, 0.9605263157894737, 0.9736842105263157, 0.9868421052631579, 1.0]

# show data in view
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.ColorArrayName = ['POINTS', 'concentration']
contour1Display.LookupTable = concentrationLUT
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'Normals'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'concentration'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'None'
contour1Display.ScaleFactor = 36.59254937733649
contour1Display.SelectScaleArray = 'concentration'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'concentration'
contour1Display.GaussianRadius = 1.8296274688668246
contour1Display.SetScaleArray = ['POINTS', 'concentration']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'concentration']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [0.75, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [0.75, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# hide data in view
Hide(porous_matrix_transport_oute1600, renderView1)

# show color bar/color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# get animation scene
animationScene1 = GetAnimationScene()

animationScene1.GoToLast()

# Properties modified on contour1
contour1.Isosurfaces = [0.75, 0.7631578947368421, 0.7763157894736842, 0.7894736842105263, 0.8026315789473684, 0.8157894736842105, 0.8289473684210527, 0.8421052631578947, 0.8552631578947368, 0.868421052631579, 0.881578947368421, 0.8947368421052632, 0.9078947368421053, 0.9210526315789473, 0.9342105263157895, 0.9473684210526316, 0.9605263157894737, 0.9736842105263157, 0.9868421052631579, 0.999]

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on contour1Display
contour1Display.Specular = 0.1

# Properties modified on contour1Display
contour1Display.Specular = 1.0

# set active source
SetActiveSource(porous_matrix_transport_oute1600)

# change representation type
porous_matrix_transport_oute1600Display.SetRepresentationType('Outline')

# set active source
SetActiveSource(porous_matrix_transport_oute1600)

# show data in view
porous_matrix_transport_oute1600Display = Show(porous_matrix_transport_oute1600, renderView1, 'UnstructuredGridRepresentation')

# show color bar/color legend
porous_matrix_transport_oute1600Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on porous_matrix_transport_oute1600
porous_matrix_transport_oute1600.SideSets = ['surface_1', 'surface_2', 'surface_3', 'surface_4']

# update the view to ensure updated data information
renderView1.Update()

# Rescale transfer function
concentrationLUT.RescaleTransferFunction(-6.383911013158657e-15, 1.0000000000000036)

# Rescale transfer function
concentrationPWF.RescaleTransferFunction(-6.383911013158657e-15, 1.0000000000000036)

# hide data in view
Hide(porous_matrix_transport_oute1600, renderView1)

# create a new 'IOSS Reader'
mesh_matrixe = IOSSReader(registrationName='mesh_matrix.e', FileName=['/home/zulianp/Desktop/code/utopiatutorials/fracnetflow/3D/benchmarks/benchmark_4/extra_large/mesh_matrix.e'])
mesh_matrixe.ElementBlocks = ['block_10']
mesh_matrixe.SideSets = []

# show data in view
mesh_matrixeDisplay = Show(mesh_matrixe, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
mesh_matrixeDisplay.Representation = 'Surface'
mesh_matrixeDisplay.ColorArrayName = [None, '']
mesh_matrixeDisplay.SelectTCoordArray = 'None'
mesh_matrixeDisplay.SelectNormalArray = 'None'
mesh_matrixeDisplay.SelectTangentArray = 'None'
mesh_matrixeDisplay.OSPRayScaleArray = 'ids'
mesh_matrixeDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
mesh_matrixeDisplay.SelectOrientationVectors = 'None'
mesh_matrixeDisplay.ScaleFactor = 140.00000000000009
mesh_matrixeDisplay.SelectScaleArray = 'None'
mesh_matrixeDisplay.GlyphType = 'Arrow'
mesh_matrixeDisplay.GlyphTableIndexArray = 'None'
mesh_matrixeDisplay.GaussianRadius = 7.0000000000000036
mesh_matrixeDisplay.SetScaleArray = ['POINTS', 'ids']
mesh_matrixeDisplay.ScaleTransferFunction = 'PiecewiseFunction'
mesh_matrixeDisplay.OpacityArray = ['POINTS', 'ids']
mesh_matrixeDisplay.OpacityTransferFunction = 'PiecewiseFunction'
mesh_matrixeDisplay.DataAxesGrid = 'GridAxesRepresentation'
mesh_matrixeDisplay.PolarAxes = 'PolarAxesRepresentation'
mesh_matrixeDisplay.ScalarOpacityUnitDistance = 4.391159830209163
mesh_matrixeDisplay.OpacityArrayName = ['POINTS', 'ids']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
mesh_matrixeDisplay.ScaleTransferFunction.Points = [223533.0, 0.0, 0.5, 0.0, 10823717.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
mesh_matrixeDisplay.OpacityTransferFunction.Points = [223533.0, 0.0, 0.5, 0.0, 10823717.0, 1.0, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# change representation type
mesh_matrixeDisplay.SetRepresentationType('Outline')

# change representation type
mesh_matrixeDisplay.SetRepresentationType('Wireframe')

# Properties modified on mesh_matrixeDisplay
mesh_matrixeDisplay.LineWidth = 2.0

# change representation type
mesh_matrixeDisplay.SetRepresentationType('Outline')

# Properties modified on mesh_matrixe
mesh_matrixe.SideSets = ['surface_1', 'surface_2', 'surface_3', 'surface_4']

# update the view to ensure updated data information
renderView1.Update()

# create a new 'IOSS Reader'
mesh_fracturee = IOSSReader(registrationName='mesh_fracture.e', FileName=['/home/zulianp/Desktop/code/utopiatutorials/fracnetflow/3D/benchmarks/benchmark_4/extra_large/mesh_fracture.e'])
mesh_fracturee.ElementBlocks = ['unnamed_block_id:_1', 'unnamed_block_id:_2', 'unnamed_block_id:_3', 'unnamed_block_id:_4', 'unnamed_block_id:_5', 'unnamed_block_id:_6', 'unnamed_block_id:_7', 'unnamed_block_id:_8', 'unnamed_block_id:_9', 'unnamed_block_id:_10', 'unnamed_block_id:_11', 'unnamed_block_id:_12', 'unnamed_block_id:_13', 'unnamed_block_id:_14', 'unnamed_block_id:_15', 'unnamed_block_id:_16', 'unnamed_block_id:_17', 'unnamed_block_id:_18', 'unnamed_block_id:_19', 'unnamed_block_id:_20', 'unnamed_block_id:_21', 'unnamed_block_id:_22', 'unnamed_block_id:_23', 'unnamed_block_id:_24', 'unnamed_block_id:_25', 'unnamed_block_id:_26', 'unnamed_block_id:_27', 'unnamed_block_id:_28', 'unnamed_block_id:_29', 'unnamed_block_id:_30', 'unnamed_block_id:_31', 'unnamed_block_id:_32', 'unnamed_block_id:_33', 'unnamed_block_id:_34', 'unnamed_block_id:_35', 'unnamed_block_id:_36', 'unnamed_block_id:_37', 'unnamed_block_id:_38', 'unnamed_block_id:_39', 'unnamed_block_id:_40', 'unnamed_block_id:_41', 'unnamed_block_id:_42', 'unnamed_block_id:_43', 'unnamed_block_id:_44', 'unnamed_block_id:_45', 'unnamed_block_id:_46', 'unnamed_block_id:_47', 'unnamed_block_id:_48', 'unnamed_block_id:_49', 'unnamed_block_id:_50', 'unnamed_block_id:_51', 'unnamed_block_id:_52']

# show data in view
mesh_fractureeDisplay = Show(mesh_fracturee, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
mesh_fractureeDisplay.Representation = 'Surface'
mesh_fractureeDisplay.ColorArrayName = [None, '']
mesh_fractureeDisplay.SelectTCoordArray = 'None'
mesh_fractureeDisplay.SelectNormalArray = 'None'
mesh_fractureeDisplay.SelectTangentArray = 'None'
mesh_fractureeDisplay.OSPRayScaleArray = 'ids'
mesh_fractureeDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
mesh_fractureeDisplay.SelectOrientationVectors = 'None'
mesh_fractureeDisplay.ScaleFactor = 140.00000000000003
mesh_fractureeDisplay.SelectScaleArray = 'None'
mesh_fractureeDisplay.GlyphType = 'Arrow'
mesh_fractureeDisplay.GlyphTableIndexArray = 'None'
mesh_fractureeDisplay.GaussianRadius = 7.000000000000001
mesh_fractureeDisplay.SetScaleArray = ['POINTS', 'ids']
mesh_fractureeDisplay.ScaleTransferFunction = 'PiecewiseFunction'
mesh_fractureeDisplay.OpacityArray = ['POINTS', 'ids']
mesh_fractureeDisplay.OpacityTransferFunction = 'PiecewiseFunction'
mesh_fractureeDisplay.DataAxesGrid = 'GridAxesRepresentation'
mesh_fractureeDisplay.PolarAxes = 'PolarAxesRepresentation'
mesh_fractureeDisplay.ScalarOpacityUnitDistance = 11.637075623360545
mesh_fractureeDisplay.OpacityArrayName = ['POINTS', 'ids']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
mesh_fractureeDisplay.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1695028.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
mesh_fractureeDisplay.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1695028.0, 1.0, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(mesh_fractureeDisplay, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
mesh_fractureeDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')
vtkBlockColorsLUT.InterpretValuesAsCategories = 1
vtkBlockColorsLUT.AnnotationsInitialized = 1
vtkBlockColorsLUT.Annotations = ['0', '0', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10', '11', '11']
vtkBlockColorsLUT.ActiveAnnotatedValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
vtkBlockColorsLUT.IndexedColors = [1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.63, 0.63, 1.0, 0.67, 0.5, 0.33, 1.0, 0.5, 0.75, 0.53, 0.35, 0.7, 1.0, 0.75, 0.5]

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.9

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.8

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.7

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.6

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.5

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.4

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.17

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.35

# turn off scalar coloring
ColorBy(mesh_fractureeDisplay, None)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(vtkBlockColorsLUT, renderView1)

# Properties modified on mesh_fractureeDisplay
mesh_fractureeDisplay.Opacity = 0.27

# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=mesh_fracturee)
calculator1.Function = ''

# Properties modified on calculator1
calculator1.Function = ''

# show data in view
calculator1Display = Show(calculator1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.ColorArrayName = [None, '']
calculator1Display.SelectTCoordArray = 'None'
calculator1Display.SelectNormalArray = 'None'
calculator1Display.SelectTangentArray = 'None'
calculator1Display.OSPRayScaleArray = 'ids'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'None'
calculator1Display.ScaleFactor = 140.00000000000003
calculator1Display.SelectScaleArray = 'None'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'None'
calculator1Display.GaussianRadius = 7.000000000000001
calculator1Display.SetScaleArray = ['POINTS', 'ids']
calculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1Display.OpacityArray = ['POINTS', 'ids']
calculator1Display.OpacityTransferFunction = 'PiecewiseFunction'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityUnitDistance = 11.637075623360545
calculator1Display.OpacityArrayName = ['POINTS', 'ids']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
calculator1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1695028.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
calculator1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1695028.0, 1.0, 0.5, 0.0]

# hide data in view
Hide(mesh_fracturee, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(calculator1Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# change representation type
calculator1Display.SetRepresentationType('Feature Edges')

# set active source
SetActiveSource(mesh_fracturee)

# show data in view
mesh_fractureeDisplay = Show(mesh_fracturee, renderView1, 'UnstructuredGridRepresentation')

# set active source
SetActiveSource(mesh_fracturee)

# set active source
SetActiveSource(calculator1)

# turn off scalar coloring
ColorBy(calculator1Display, None)

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(vtkBlockColorsLUT, renderView1)

# change representation type
calculator1Display.SetRepresentationType('Wireframe')

# Properties modified on calculator1Display
calculator1Display.LineWidth = 2.0

# change representation type
calculator1Display.SetRepresentationType('Feature Edges')

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1610, 779)

# current camera placement for renderView1
renderView1.CameraPosition = [692.9604698398091, -893.5953544683035, 2273.930661370096]
renderView1.CameraFocalPoint = [-133.49063889582013, 786.2779266112324, 212.26596593290137]
renderView1.CameraViewUp = [0.951622718131113, 0.25146734582833646, -0.17657399672319876]
renderView1.CameraParallelScale = 872.1381771256208

# save animation
SaveAnimation('/home/zulianp/Desktop/code/utopiatutorials/fracnetflow/3D/benchmarks/benchmark_4/extra_large/movie/transport.png', renderView1, ImageResolution=[3220, 1558],
    FrameWindow=[0, 29], 
    # PNG options
    CompressionLevel='0')

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(1610, 779)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.CameraPosition = [692.9604698398091, -893.5953544683035, 2273.930661370096]
renderView1.CameraFocalPoint = [-133.49063889582013, 786.2779266112324, 212.26596593290137]
renderView1.CameraViewUp = [0.951622718131113, 0.25146734582833646, -0.17657399672319876]
renderView1.CameraParallelScale = 872.1381771256208

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).