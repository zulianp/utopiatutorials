#!/usr/bin/env bash

set -e
set -x

TRILINOS_DIR=$INSTALL_DIR/Trilinos
PATH=$PATH:$TRILINOS_DIR/bin

perceptX refine ../large/mesh_matrix.e mesh_matrix.e
perceptX refine ../large/mesh_fracture.e mesh_fracture.e
