#!/usr/bin/env bash

set -e
set -x

PATH=$PATH:$INSTALL_DIR/Trilinos/bin

p=16

# decomp -p $p mesh_matrix.e
# decomp -p $p mesh_fracture.e

# mpiexec -np $p $UTOPIA_FE_EXEC @file ../flow.yaml 
mpiexec -np $p $UTOPIA_FE_EXEC @file ../transport.yaml

# rm mesh_matrix.e."$p".*
# rm mesh_fracture.e."$p".*

epu -auto porous_matrix_out.e."$p".00
epu -auto fracture_network_out.e."$p".00

# rm porous_matrix_out.e."$p".*
# rm fracture_network_out.e."$p".*
