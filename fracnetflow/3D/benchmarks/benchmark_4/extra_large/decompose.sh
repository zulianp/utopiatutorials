#!/usr/bin/env bash

set -e
set -x

# TRILINOS_DIR=$INSTALL_DIR/Trilinos
PATH=$PATH:$TRILINOS_DIR/bin


nn=42
p=$((nn *  36))

srun decomp -p $p mesh_matrix.e
srun decomp -p $p mesh_fracture.e
