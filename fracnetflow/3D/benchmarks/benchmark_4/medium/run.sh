#!/bin/bash

echo "Running: "
echo $UTOPIA_FE_EXEC' @file ../flow.yaml && '$UTOPIA_FE_EXEC' @file ../transport.yaml'
echo ""

$UTOPIA_FE_EXEC @file ../flow.yaml && $UTOPIA_FE_EXEC @file ../transport.yaml