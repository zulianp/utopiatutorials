#!/usr/bin/env bash

ffmpeg -framerate 10 -pattern_type glob -i '*.png' \
   -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" \
   -c:v libx264 -pix_fmt yuv420p movie.mp4